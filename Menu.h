#include "SFML/Graphics.hpp"

#define MAX_NUMBER_OF_ITEMS 8

class Menu
{
public:
	Menu(std::string s1, std::string s2, std::string s3, std::string s4, std::string s5, std::string s6, std::string s7, std::string s8);
	~Menu();

	void draw(sf::RenderWindow &window);
	void MoveUp();
	void MoveDown();
	int GetPressedItem() { return nu; }

private:
	int nu;
	sf::Font font;
	sf::Text menu[MAX_NUMBER_OF_ITEMS];

};
