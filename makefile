all:
	g++ main.cpp Menu.cpp -o sfml-app \
	-std=c++11 -lsfml-graphics -lsfml-window -lsfml-system \
	-Werror=narrowing

run:
	sfml-app
