#include "Menu.h"

Menu::Menu(std::string s1, std::string s2, std::string s3, std::string s4, std::string s5, std::string s6, std::string s7, std::string s8)
{
	if (!font.loadFromFile("arial.ttf"))
	{
        // error...
	}

	menu[0].setFont(font);
	menu[0].setFillColor(sf::Color::Red);
	menu[0].setString(s1);
	menu[0].setPosition(sf::Vector2f(50, 50));

	menu[1].setFont(font);
	menu[1].setFillColor(sf::Color::White);
	menu[1].setString(s2);
	menu[1].setPosition(sf::Vector2f(50, 150));


  menu[2].setFont(font);
	menu[2].setFillColor(sf::Color::White);
	menu[2].setString(s3);
	menu[2].setPosition(sf::Vector2f(50, 250));

  menu[3].setFont(font);
	menu[3].setFillColor(sf::Color::White);
	menu[3].setString(s4);
	menu[3].setPosition(sf::Vector2f(50, 350));

  menu[4].setFont(font);
	menu[4].setFillColor(sf::Color::White);
	menu[4].setString(s5);
	menu[4].setPosition(sf::Vector2f(50, 450));

  menu[5].setFont(font);
	menu[5].setFillColor(sf::Color::White);
	menu[5].setString(s6);
	menu[5].setPosition(sf::Vector2f(50, 550));

  menu[6].setFont(font);
	menu[6].setFillColor(sf::Color::White);
	menu[6].setString(s7);
	menu[6].setPosition(sf::Vector2f(50, 650));

	menu[7].setFont(font);
	menu[7].setFillColor(sf::Color::White);
	menu[7].setString(s8);
	menu[7].setPosition(sf::Vector2f(50, 750));

	nu = 0;
}


Menu::~Menu()
{
}

void Menu::draw(sf::RenderWindow &window)
{
	for (int i = 0; i < MAX_NUMBER_OF_ITEMS; i++)
	{
		window.draw(menu[i]);
	}
}

void Menu::MoveUp()
{
	if (nu - 1 >= 0)
	{
        menu[nu].setFillColor(sf::Color::White);
		nu--;
		menu[nu].setFillColor(sf::Color::Red);
	}
}

void Menu::MoveDown()
{
	if (nu + 1 < MAX_NUMBER_OF_ITEMS)
	{
		menu[nu].setFillColor(sf::Color::White);
		nu++;
		menu[nu].setFillColor(sf::Color::Red);
	}
}
