#include <iostream>
#include <string>
#include <cstdio>
#include <math.h>
#include <random>
#include <ctime>
#include "exprtk.hpp"
#include "Menu.h"
#include "SFML/Graphics.hpp"

int n = 100;   //liczba iteracjil; czas wykonywania rosnie wprostproporcjonalnie
double lower = 0.0;
double upper = 4.0;
double lowerx = 0.0;
double upperx = 4.0;
double lowery = 0.0;
double uppery = 4.0;


std::string expr_str = "y+x*2+1";

double func(double x){
  typedef exprtk::symbol_table<double> symbol_table_t;
  typedef exprtk::expression<double>     expression_t;
  typedef exprtk::parser<double>             parser_t;
  std::string expression_str = expr_str;

  // Register x with the symbol_table
  symbol_table_t symbol_table;
  symbol_table.add_variable("x",x);

  // Instantiate expression and register symbol_table
  expression_t expression;
  expression.register_symbol_table(symbol_table);

  // Instantiate parser and compile the expression
  parser_t parser;
  parser.compile(expression_str,expression);

  double result = 0.0;

  result = expression.value();

  return result;
}
double func2(double x, double y){
    typedef exprtk::symbol_table<double> symbol_table_t;
    typedef exprtk::expression<double> expression_t;
    typedef exprtk::parser<double> parser_t;
    typedef exprtk::parser_error::type error_t;

    std::string expression_str = expr_str;

    symbol_table_t symbol_table;
    symbol_table.add_constants();
    symbol_table.add_variable("x",x);
    symbol_table.add_variable("y",y);

    expression_t expression;
    expression.register_symbol_table(symbol_table);

    parser_t parser;
    parser.compile(expression_str,expression);

    double result = expression.value();

    return result;
}
double difprim(double x){       //rózniczka 1 stopnia (do eulera)
	double s = 0.0;
    double h = 0.005;
    s =(func(x + h) - func(x)) / h;
    return s;
}
double difter   (double x){     //rózniczka 3 stopnia (do eulera)
    double s = 0.0;
    double h = 0.005;
    s =(func(x + 2 * h) - func(x - 2 * h) -  2 * func(x + h) + 2 * func(x - h)) / (2.0 * pow(h, 3));
    return s;
}
double trapezoidal(double a, double b, int n, bool ret){
    clock_t begin = clock();
    double s = 0.0;
    double x = a;
    double h = (b - a) / (1.0 * n);
    for(int i=0; i<=n; i++){
        if((i == 0) || (i == n)){
            s += func(x)/2.0;
        }
        else{
            s += func(x);
        }
        x = x + h;
    }
    s *= h;
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if(ret == 0)
        return s;
    else
        return elapsed_secs;
}
double simpson(double a, double b, int n, bool ret){
    clock_t begin = clock();
    double s = 0.0;
    double x = a;
    double h = (b - a) / (1.0 * n);
    for(int i=0; i<=n; i++){
        if((i == 0) || (i == n)){
            s += func(x);
        }
        else if(i % 2 == 0 ){
            s += 2.0 * func(x);
        }
        else{
            s += 4.0 * func(x);
        }
        x += h;
    }
    s = s * h / 3.0;
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if(ret == 0)
        return s;
    else
        return elapsed_secs;

}
double boole(double a, double b, int n, bool ret){
    clock_t begin = clock();
    double s = 0.0;
    double x = a;
    double h = (b - a) / (1.0 * n);
    for(int i=0; i<=n; i++){
        if((i == 0) || (i == n)){
            s += 7.0 * func(x);
        }
        else if(i % 2 != 0 ){
            s += 32.0 * func(x);
        }
        else if(i % 4 == 0){
            s += 14.0 * func(x);
        }
        else{
            s += 12.0 * func(x);
        }
        x += h;
    }
    s = s * 2.0 * h / 45.0;
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if(ret == 0)
        return s;
    else
        return elapsed_secs;
}
double euler(double a, double b, int n, bool ret){
    clock_t begin = clock();
    double s = 0.0;
    double x = a;
    double h = (b - a) / (1.0 * n);
    for(int i=0; i<=n; i++){
        if((i == 0) || (i == n)){
            s += func(x) / 2.0;
        }
        else{
            s += func(x);
        }
        x += h;
    }
    s *= h;
    s += (pow(h, 2) / 12.0) * (difprim(a) - difprim(b)) - (pow(h, 4) / 720.0) * (difter(a) - difter(b));
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if(ret == 0)
        return s;
    else
        return elapsed_secs;
}
double monte_carlo(double a, double b, int n, bool ret){
    clock_t begin = clock();
    std::random_device rd;                         // obtain a random number from hardware
    std::mt19937 eng(rd());                        // seed the generator
    std::uniform_real_distribution<> distr(a, b); // define the range
    double s = 0.0;
    double rdm = 0.0;
    for(int i=1;i<=n;i++){
        s += func(distr(eng));
    }
    s /= n;
    s *= (b-a);
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

    if(ret == 0)
        return s;
    else
        return elapsed_secs;


}
double midpoint(double a, double b, int n, bool ret){
    clock_t begin = clock();
    double s = 0.0;
    double x = a;
    double h = (b - a) / (1.0 * n);
    for(int i=0; i<=n; i++){
        s += func(x + (2 * i - 1) * h / 2.0);
    }
    s *= h;
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if(ret == 0)
        return s;
    else
        return elapsed_secs;
}
double trapezoidal2(double a, double b, double c, double d, int n, bool ret){
    clock_t begin = clock();
    double s = 0.0;
    double x = a;
    double y = c;
    double hx = (b - a) / (1.0 * n);
    double hy = (d - c) / (1.0 * n);
    s += func2(a,c);
    s += func2(a,d);
    s += func2(b,c);
    s += func2(b,d);
    for(int i=0; i<=n; i++){
      x = a;
      for(int j=0; j<=n; j++){
        s += 4 * func2(x,y);
        x += hx;
      }
      y += hy;
    }
    y = c;
    for(int ii=0; ii<=n; ii++){
        s += 2*(func2(a, y) + func2(b, y));
        y += hy;
    }
    x = a;
    for(int jj=0; jj<=n; jj++){
        s += 2*(func2(x, c) + func2(x, d));
        x += hx;
    }
    s = s * hx * hy / 4.0;
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if(ret == 0)
        return s;
    else
        return elapsed_secs;
}
double simpson2(double a, double b, double c, double d, int n, bool ret){
    clock_t begin = clock();
    double s = 0.0;
    double x = a;
    double y = c;
    double hx = (b - a) / (1.0 * n);
    double hy = (d - c) / (1.0 * n);
    s += func2(a,c);
    s += func2(a,d);
    s += func2(b,c);
    s += func2(b,d);
    y = c;
    for(int ii=0; ii<=n; ii++){
        s += 6*(func2(a, y) + func2(b, y));
        y += hy;
    }
    x = a;
    for(int jj=0; jj<=n; jj++){
        s += 6*(func2(x, c) + func2(x, d));
        x += hx;
    }
    y = c;
    for(int i=0; i<=n; i++){
      x = a;
      for(int j=0; j<=n; j++){
        s += 22 * func2(x,y);
        x += hx;
      }
      y += hy;
    }
    s = s * hx * hy / 9.0;
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    if(ret == 0)
        return s;
    else
        return elapsed_secs;
}
int main()
{
    sf::Font font;
    if (!font.loadFromFile("arial.ttf"))
    {
        // error...
    }
    // main window
  	sf::RenderWindow window(sf::VideoMode(1000, 850), "Num-integrator");

  	// main menu
  	Menu menu("Funkcja", "Trapezoid", "Simpson", "Boole", "Euler", "Monte Carlo", "Midpoint", "Wyjscie");
    Menu menu2("Funkcja", "Trapezoid", "Simpson", "-", "-", "-", "-", "Wyjscie");


    // expresion Text
    bool calk = 0;
    sf::Text ex;
    ex.setFont(font);
    ex.setCharacterSize(40);
    ex.setFillColor(sf::Color(255, 255, 255));
    ex.setPosition(250, 60);
    ex.setString("current f(x) = " + expr_str + "\nbounds from " + std::to_string(lower) + " to " + std::to_string(upper));

    // integral Text
    sf::Text cal;
    cal.setFont(font);
    cal.setCharacterSize(50);
    cal.setFillColor(sf::Color(255, 255, 255));
    cal.setPosition(450, 210);
    cal.setString(L"value of ∫f(x)dx");

    // value Text
    sf::Text va;
    va.setFont(font);
    va.setCharacterSize(50);
    va.setFillColor(sf::Color(255, 255, 255));
    va.setPosition(450, 260);

    // elapsed time Text
    sf::Text tim;
    tim.setFont(font);
    tim.setCharacterSize(50);
    tim.setFillColor(sf::Color(255, 255, 255));
    tim.setPosition(450, 500);

    bool sum = 0;
    bool time = 1;
    std::string expr;
    double lw;
    double up;
    double lwx;
    double upx;
    double lwy;
    double upy;
    std::string yy = "y";

    while (window.isOpen())
        {
            sf::Event event;
            while (window.pollEvent(event))
            {
                switch (event.type)
                {
                case sf::Event::KeyReleased:
                    switch (event.key.code)
                    {
                    case sf::Keyboard::Up:
                        menu.MoveUp();
                        menu2.MoveUp();
                        break;

                    case sf::Keyboard::Down:
                        menu.MoveDown();
                        menu2.MoveDown();
                        break;

                    case sf::Keyboard::Return:
                        calk = 1;
                        switch (menu.GetPressedItem())
                        {
                        case 0:{
                          if(expr_str.find(yy) != std::string::npos){
                              std::cout << "enter lower x" << std::endl;
                              std::cin >> lwx;
                              std::cout << "enter upper x" << std::endl;
                              std::cin >> upx;
                              std::cout << "enter lower y" << std::endl;
                              std::cin >> lwy;
                              std::cout << "enter upper y" << std::endl;
                              std::cin >> upy;
                              std::cout << "enter expresion" << std::endl;
                              std::cin >> expr;

                              expr_str = expr;
                              lowerx = lwx;
                              upperx = upx;
                              lowery = lwy;
                              uppery = upy;

                              ex.setString("current f(x,y) = " + expr_str + "\nx bounds from " + std::to_string(lowerx) + " to " + std::to_string(upperx) + "\ny bounds from " + std::to_string(lowery) + " to " + std::to_string(uppery));
                              cal.setString(L"value of ∫∫f(x,y)dxdy");
                          }
                          else{
                              std::cout << "enter lower" << std::endl;
                              std::cin >> lw;
                              std::cout << "enter upper" << std::endl;
                              std::cin >> up;
                              std::cout << "enter expresion" << std::endl;
                              std::cin >> expr;

                              expr_str = expr;
                              lower = lw;
                              upper = up;

                              ex.setString("current f(x) = " + expr_str + "\nbounds from " + std::to_string(lower) + " to " + std::to_string(upper));
                              }
                            }break;
                            case 1:{
                                if(expr_str.find(yy) != std::string::npos){
                                    va.setString(std::to_string(trapezoidal2(lowerx, upperx, lowery, uppery, n, sum)));
                                    tim.setString("elapsed time in sec:\n" + std::to_string(trapezoidal2(lowerx, upperx, lowery, uppery, n, time)));
                                }
                                else{
                                    va.setString(std::to_string(trapezoidal(lower, upper, n, sum)));
                                    tim.setString("elapsed time in sec:\n" + std::to_string(trapezoidal(lower, upper, n, time)));
                                }
                                }break;
                            case 2:{
                                if(expr_str.find(yy) != std::string::npos){
                                    va.setString(std::to_string(simpson2(lowerx, upperx, lowery, uppery, n, sum)));
                                    tim.setString("elapsed time in sec:\n" + std::to_string(simpson2(lowerx, upperx, lowery, uppery, n, time)));
                                }
                                else{
                                va.setString(std::to_string(simpson(lower, upper, n, sum)));
                                tim.setString("elapsed time in sec:\n" + std::to_string(simpson(lower, upper, n, time)));
                                }
                                }break;
                            case 3:{
                                va.setString(std::to_string(boole(lower, upper, n, sum)));
                                tim.setString("elapsed time in sec:\n" + std::to_string(boole(lower, upper, n, time)));
                                }break;
                            case 4:{
                                va.setString(std::to_string(euler(lower, upper, n, sum)));
                                tim.setString("elapsed time in sec:\n" + std::to_string(euler(lower, upper, n, time)));
                                }break;
                            case 5:{
                                va.setString(std::to_string(monte_carlo(lower, upper, n, sum)));
                                tim.setString("elapsed time in sec:\n" + std::to_string(monte_carlo(lower, upper, n, time)));
                                }break;
                            case 6:{
                                va.setString(std::to_string(midpoint(lower, upper, n, sum)));
                                tim.setString("elapsed time in sec:\n" + std::to_string(midpoint(lower, upper, n, time)));
                                }break;
                            case 7:
                                window.close();
                                break;
                            }break;

                        }
                        break;
                    case sf::Event::Closed:
                        window.close();
                        break;
                    }
                }
                window.clear();
                window.draw(tim);
                window.draw(va);
                window.draw(ex);
                if(calk == 1){
                    window.draw(cal);
                }
                if(expr_str.find(yy) == std::string::npos){
                    menu.draw(window);
                }
                else{
                    menu2.draw(window);
                }
                window.display();

    }

}
